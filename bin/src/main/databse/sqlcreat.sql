CREATE DATABASE  IF NOT EXISTS `klassenbuch` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `klassenbuch`;
-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: gyha_klassenbuch
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `absenz`
--

DROP TABLE IF EXISTS `absenz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `absenz` (
  `idabsenz` int(11) NOT NULL AUTO_INCREMENT,
  `idklassenbucheintrag` int(11) NOT NULL,
  `ist_entschuldigt` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idabsenz`,`idklassenbucheintrag`),
  KEY `absenz_klassenbucheintrag_fk_idx` (`idklassenbucheintrag`),
  CONSTRAINT `absenz_klassenbucheintrag_fk` FOREIGN KEY (`idklassenbucheintrag`) REFERENCES `klassenbucheintrag` (`idklassenbucheintrag`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `absenz`
--

LOCK TABLES `absenz` WRITE;
/*!40000 ALTER TABLE `absenz` DISABLE KEYS */;
/*!40000 ALTER TABLE `absenz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disziplinareintrag`
--

DROP TABLE IF EXISTS `disziplinareintrag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disziplinareintrag` (
  `iddisziplinareintrag` int(11) NOT NULL AUTO_INCREMENT,
  `idklassenbucheintrag` int(11) NOT NULL,
  `beschreibung` text,
  PRIMARY KEY (`iddisziplinareintrag`,`idklassenbucheintrag`),
  KEY `diszipliareintrag_klassenbucheintrag_fk_idx` (`idklassenbucheintrag`),
  CONSTRAINT `diszipliareintrag_klassenbucheintrag_fk` FOREIGN KEY (`idklassenbucheintrag`) REFERENCES `klassenbucheintrag` (`idklassenbucheintrag`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disziplinareintrag`
--

LOCK TABLES `disziplinareintrag` WRITE;
/*!40000 ALTER TABLE `disziplinareintrag` DISABLE KEYS */;
/*!40000 ALTER TABLE `disziplinareintrag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fach`
--

DROP TABLE IF EXISTS `fach`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fach` (
  `idfach` int(11) NOT NULL AUTO_INCREMENT,
  `bezeichnung` varchar(30) NOT NULL,
  `kuerzel` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idfach`),
  UNIQUE KEY `bezeichnung_UNIQUE` (`bezeichnung`),
  UNIQUE KEY `kuerzel_UNIQUE` (`kuerzel`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fach`
--

LOCK TABLES `fach` WRITE;
/*!40000 ALTER TABLE `fach` DISABLE KEYS */;
INSERT INTO `fach` VALUES (1,'Mathematik','mat'),(2,'Deutsch','deu'),(3,'Englisch','eng');
/*!40000 ALTER TABLE `fach` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fehlende_hausaufgaben`
--

DROP TABLE IF EXISTS `fehlende_hausaufgaben`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fehlende_hausaufgaben` (
  `idfehlende_hausaufgaben` int(11) NOT NULL AUTO_INCREMENT,
  `idklassenbucheintrag_fk` int(11) NOT NULL,
  PRIMARY KEY (`idfehlende_hausaufgaben`,`idklassenbucheintrag_fk`),
  KEY `hausaufgaben_klassenbucheintrag_fk_idx` (`idklassenbucheintrag_fk`),
  CONSTRAINT `hausaufgaben_klassenbucheintrag_fk` FOREIGN KEY (`idklassenbucheintrag_fk`) REFERENCES `klassenbucheintrag` (`idklassenbucheintrag`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fehlende_hausaufgaben`
--

LOCK TABLES `fehlende_hausaufgaben` WRITE;
/*!40000 ALTER TABLE `fehlende_hausaufgaben` DISABLE KEYS */;
/*!40000 ALTER TABLE `fehlende_hausaufgaben` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hausaufgaben`
--

DROP TABLE IF EXISTS `hausaufgaben`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hausaufgaben` (
  `idhausaufgaben` int(11) NOT NULL,
  `idklassenbucheintrag` int(11) NOT NULL,
  `beschreibung` text,
  `faelligkeit` date DEFAULT NULL,
  PRIMARY KEY (`idhausaufgaben`,`idklassenbucheintrag`),
  KEY `hausaufgaben_klassenbucheintrag_fk_idx` (`idklassenbucheintrag`),
  CONSTRAINT `hausaufgabe_klassenbucheintrag_fk` FOREIGN KEY (`idklassenbucheintrag`) REFERENCES `klassenbucheintrag` (`idklassenbucheintrag`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hausaufgaben`
--

LOCK TABLES `hausaufgaben` WRITE;
/*!40000 ALTER TABLE `hausaufgaben` DISABLE KEYS */;
/*!40000 ALTER TABLE `hausaufgaben` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `klasse`
--

DROP TABLE IF EXISTS `klasse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `klasse` (
  `idklasse` int(11) NOT NULL AUTO_INCREMENT,
  `stufe` int(11) NOT NULL,
  `bezeichnung` varchar(10) NOT NULL,
  PRIMARY KEY (`idklasse`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `klasse`
--

LOCK TABLES `klasse` WRITE;
/*!40000 ALTER TABLE `klasse` DISABLE KEYS */;
INSERT INTO `klasse` VALUES (1,5,'5a'),(2,5,'5a'),(3,5,'5a'),(4,5,'5a'),(5,5,'5a');
/*!40000 ALTER TABLE `klasse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `klasse_ist_unterrichtseinheit`
--

DROP TABLE IF EXISTS `klasse_ist_unterrichtseinheit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `klasse_ist_unterrichtseinheit` (
  `idklasse_fk` int(11) NOT NULL,
  `idunterrichtseinheit_fk` int(11) NOT NULL,
  PRIMARY KEY (`idklasse_fk`,`idunterrichtseinheit_fk`),
  KEY `unterrichtseinheit_klasse_fk_idx` (`idunterrichtseinheit_fk`),
  CONSTRAINT `klasse_unterrichtseinheit_fk` FOREIGN KEY (`idklasse_fk`) REFERENCES `klasse` (`idklasse`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `unterrichtseinheit_klasse_fk` FOREIGN KEY (`idunterrichtseinheit_fk`) REFERENCES `unterrichtseinheit` (`idunterrichtseinheit`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `klasse_ist_unterrichtseinheit`
--

LOCK TABLES `klasse_ist_unterrichtseinheit` WRITE;
/*!40000 ALTER TABLE `klasse_ist_unterrichtseinheit` DISABLE KEYS */;
INSERT INTO `klasse_ist_unterrichtseinheit` VALUES (1,46),(1,47);
/*!40000 ALTER TABLE `klasse_ist_unterrichtseinheit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `klassenbucheintrag`
--

DROP TABLE IF EXISTS `klassenbucheintrag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `klassenbucheintrag` (
  `idklassenbucheintrag` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lehrer_fk` int(11) NOT NULL,
  `schueler_fk` int(11) DEFAULT NULL,
  `stundenplaneinheit_fk` int(11) NOT NULL,
  PRIMARY KEY (`idklassenbucheintrag`),
  KEY `klassenbucheintrag_lehrer_fk_idx` (`lehrer_fk`),
  KEY `klassenbucheintrag_schueler_fk_idx` (`schueler_fk`),
  KEY `klassenbucheintrag_unterrichtseinheit_fk_idx` (`stundenplaneinheit_fk`),
  CONSTRAINT `klassenbucheintrag_lehrer_fk` FOREIGN KEY (`lehrer_fk`) REFERENCES `lehrer` (`idlehrer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `klassenbucheintrag_schueler_fk` FOREIGN KEY (`schueler_fk`) REFERENCES `schueler` (`idschueler`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `klassenbucheintrag_unterrichtseinheit_fk` FOREIGN KEY (`stundenplaneinheit_fk`) REFERENCES `unterrichtseinheit` (`idunterrichtseinheit`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `klassenbucheintrag`
--

LOCK TABLES `klassenbucheintrag` WRITE;
/*!40000 ALTER TABLE `klassenbucheintrag` DISABLE KEYS */;
/*!40000 ALTER TABLE `klassenbucheintrag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kurs`
--

DROP TABLE IF EXISTS `kurs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kurs` (
  `idkurs` int(11) NOT NULL AUTO_INCREMENT,
  `bezeichnung` varchar(45) NOT NULL,
  `unterrichtseinheit_fk` int(11) NOT NULL,
  PRIMARY KEY (`idkurs`),
  KEY `kurs_unterrichtseinheit_fk_idx` (`unterrichtseinheit_fk`),
  CONSTRAINT `kurs_unterrichtseinheit_fk` FOREIGN KEY (`unterrichtseinheit_fk`) REFERENCES `unterrichtseinheit` (`idunterrichtseinheit`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kurs`
--

LOCK TABLES `kurs` WRITE;
/*!40000 ALTER TABLE `kurs` DISABLE KEYS */;
INSERT INTO `kurs` VALUES (1,'tstmathe',48);
/*!40000 ALTER TABLE `kurs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kurs_stufen`
--

DROP TABLE IF EXISTS `kurs_stufen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kurs_stufen` (
  `idkurs_fk` int(11) NOT NULL,
  `idstufe_fk` int(11) NOT NULL,
  PRIMARY KEY (`idkurs_fk`,`idstufe_fk`),
  KEY `fk_kurs_stufen_stufen_idx` (`idstufe_fk`),
  CONSTRAINT `fk_kurs_stufen_kurs` FOREIGN KEY (`idkurs_fk`) REFERENCES `kurs` (`idkurs`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_kurs_stufen_stufen` FOREIGN KEY (`idstufe_fk`) REFERENCES `stufe` (`idstufe`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kurs_stufen`
--

LOCK TABLES `kurs_stufen` WRITE;
/*!40000 ALTER TABLE `kurs_stufen` DISABLE KEYS */;
INSERT INTO `kurs_stufen` VALUES (1,1),(1,2);
/*!40000 ALTER TABLE `kurs_stufen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lehrer`
--

DROP TABLE IF EXISTS `lehrer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lehrer` (
  `idlehrer` int(11) NOT NULL AUTO_INCREMENT,
  `kuerzel` varchar(10) NOT NULL,
  `vorname` varchar(45) NOT NULL,
  `nachname` varchar(45) NOT NULL,
  `geburtsdatum` date DEFAULT NULL,
  `typ` varchar(30) NOT NULL,
  `interne_verwaltungsnummer` int(11) DEFAULT NULL,
  `passwort` varchar(45) NOT NULL,
  PRIMARY KEY (`idlehrer`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lehrer`
--

LOCK TABLES `lehrer` WRITE;
/*!40000 ALTER TABLE `lehrer` DISABLE KEYS */;
INSERT INTO `lehrer` VALUES (1,'kot','johannes','kotsch','1970-01-01','lehrer',1,'qq'),(2,'kt','k','kok','2017-08-06','lehrer',0,'0000'),(3,'test','test','test','2017-08-10','lehrer',0,'0000'),(4,'ww','ww','ww','2017-08-14','lehrer',0,'0000');
/*!40000 ALTER TABLE `lehrer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lehrer_ist_klassenlehrer`
--

DROP TABLE IF EXISTS `lehrer_ist_klassenlehrer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lehrer_ist_klassenlehrer` (
  `klassenlehrer_lehrer_fk` int(11) NOT NULL,
  `klassenlehrer_klasse_fk` int(11) NOT NULL,
  PRIMARY KEY (`klassenlehrer_lehrer_fk`,`klassenlehrer_klasse_fk`),
  KEY `klassenlehrer_klasse_fk_idx` (`klassenlehrer_klasse_fk`),
  CONSTRAINT `klassenlehrer_klasse_fk` FOREIGN KEY (`klassenlehrer_klasse_fk`) REFERENCES `klasse` (`idklasse`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `klassenlehrer_lehrer_fk` FOREIGN KEY (`klassenlehrer_lehrer_fk`) REFERENCES `lehrer` (`idlehrer`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lehrer_ist_klassenlehrer`
--

LOCK TABLES `lehrer_ist_klassenlehrer` WRITE;
/*!40000 ALTER TABLE `lehrer_ist_klassenlehrer` DISABLE KEYS */;
INSERT INTO `lehrer_ist_klassenlehrer` VALUES (1,2),(1,3);
/*!40000 ALTER TABLE `lehrer_ist_klassenlehrer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lehrer_ist_tutor`
--

DROP TABLE IF EXISTS `lehrer_ist_tutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lehrer_ist_tutor` (
  `tutor_lehrer_fk` int(11) NOT NULL,
  `tutor_schueler_fk` int(11) NOT NULL,
  PRIMARY KEY (`tutor_lehrer_fk`,`tutor_schueler_fk`),
  KEY `tutor_schueler_fk_idx` (`tutor_schueler_fk`),
  CONSTRAINT `tutor_lehrer_fk` FOREIGN KEY (`tutor_lehrer_fk`) REFERENCES `lehrer` (`idlehrer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tutor_schueler_fk` FOREIGN KEY (`tutor_schueler_fk`) REFERENCES `schueler` (`idschueler`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lehrer_ist_tutor`
--

LOCK TABLES `lehrer_ist_tutor` WRITE;
/*!40000 ALTER TABLE `lehrer_ist_tutor` DISABLE KEYS */;
/*!40000 ALTER TABLE `lehrer_ist_tutor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lehrer_unterrichtet_unterrichtseinheit`
--

DROP TABLE IF EXISTS `lehrer_unterrichtet_unterrichtseinheit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lehrer_unterrichtet_unterrichtseinheit` (
  `lehrer_fk` int(11) NOT NULL,
  `unterrichtseinheit_fk` int(11) NOT NULL,
  PRIMARY KEY (`lehrer_fk`,`unterrichtseinheit_fk`),
  KEY `unterrichtet_unterrichtseinheit_fk_idx` (`unterrichtseinheit_fk`),
  CONSTRAINT `unterrichtet_lehrer_fk` FOREIGN KEY (`lehrer_fk`) REFERENCES `lehrer` (`idlehrer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `unterrichtet_unterrichtseinheit_fk` FOREIGN KEY (`unterrichtseinheit_fk`) REFERENCES `unterrichtseinheit` (`idunterrichtseinheit`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lehrer_unterrichtet_unterrichtseinheit`
--

LOCK TABLES `lehrer_unterrichtet_unterrichtseinheit` WRITE;
/*!40000 ALTER TABLE `lehrer_unterrichtet_unterrichtseinheit` DISABLE KEYS */;
INSERT INTO `lehrer_unterrichtet_unterrichtseinheit` VALUES (1,46),(1,48),(1,49);
/*!40000 ALTER TABLE `lehrer_unterrichtet_unterrichtseinheit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raum`
--

DROP TABLE IF EXISTS `raum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raum` (
  `idraum` int(11) NOT NULL AUTO_INCREMENT,
  `bezeichnung` varchar(45) NOT NULL,
  PRIMARY KEY (`idraum`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raum`
--

LOCK TABLES `raum` WRITE;
/*!40000 ALTER TABLE `raum` DISABLE KEYS */;
INSERT INTO `raum` VALUES (1,'r1'),(2,'r1'),(3,'r2');
/*!40000 ALTER TABLE `raum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schueler`
--

DROP TABLE IF EXISTS `schueler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schueler` (
  `idschueler` int(11) NOT NULL AUTO_INCREMENT,
  `interne_verwaltungsnummer` int(11) DEFAULT NULL,
  `geburtsdatum` date NOT NULL,
  `vorname` varchar(45) NOT NULL,
  `nachname` varchar(45) NOT NULL,
  `klasse_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`idschueler`),
  KEY `fk_schueler_klasse_idx` (`klasse_fk`),
  CONSTRAINT `fk_schueler_klasse` FOREIGN KEY (`klasse_fk`) REFERENCES `klasse` (`idklasse`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schueler`
--

LOCK TABLES `schueler` WRITE;
/*!40000 ALTER TABLE `schueler` DISABLE KEYS */;
INSERT INTO `schueler` VALUES (6,2,'2017-08-18','aab','qq',1),(7,2,'2017-08-18','aa','qq',1),(8,2,'2017-08-18','aa','qq',1),(9,2,'2017-08-18','aa','qq',1),(21,1,'2017-08-19','e','e',1),(22,1,'2017-08-19','e','e',1),(23,1,'2017-08-19','e','e',1),(24,1,'2017-08-19','e','e',1),(25,1,'2017-08-19','e','e',1),(26,1,'2017-08-19','e','e',1);
/*!40000 ALTER TABLE `schueler` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schueler_besucht_kurs`
--

DROP TABLE IF EXISTS `schueler_besucht_kurs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schueler_besucht_kurs` (
  `schueler_fk` int(11) NOT NULL,
  `kurs_fk` int(11) NOT NULL,
  PRIMARY KEY (`schueler_fk`,`kurs_fk`),
  KEY `besucht_kurs_fk_idx` (`kurs_fk`),
  CONSTRAINT `besucht_kurs_fk` FOREIGN KEY (`kurs_fk`) REFERENCES `kurs` (`idkurs`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `besucht_schueler_fk` FOREIGN KEY (`schueler_fk`) REFERENCES `schueler` (`idschueler`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schueler_besucht_kurs`
--

LOCK TABLES `schueler_besucht_kurs` WRITE;
/*!40000 ALTER TABLE `schueler_besucht_kurs` DISABLE KEYS */;
/*!40000 ALTER TABLE `schueler_besucht_kurs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schueler_ist_in_klasse`
--

DROP TABLE IF EXISTS `schueler_ist_in_klasse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schueler_ist_in_klasse` (
  `ist_in_schueler_fk` int(11) NOT NULL,
  `ist_in_klasse_fk` int(11) NOT NULL,
  PRIMARY KEY (`ist_in_schueler_fk`,`ist_in_klasse_fk`),
  KEY `ist_in_klasse_fk_idx` (`ist_in_klasse_fk`),
  CONSTRAINT `ist_in_klasse_fk` FOREIGN KEY (`ist_in_klasse_fk`) REFERENCES `klasse` (`idklasse`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ist_in_schueler_fk` FOREIGN KEY (`ist_in_schueler_fk`) REFERENCES `schueler` (`idschueler`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schueler_ist_in_klasse`
--

LOCK TABLES `schueler_ist_in_klasse` WRITE;
/*!40000 ALTER TABLE `schueler_ist_in_klasse` DISABLE KEYS */;
/*!40000 ALTER TABLE `schueler_ist_in_klasse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stufe`
--

DROP TABLE IF EXISTS `stufe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stufe` (
  `idstufe` int(11) NOT NULL AUTO_INCREMENT,
  `bezeichnung` varchar(20) NOT NULL,
  PRIMARY KEY (`idstufe`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stufe`
--

LOCK TABLES `stufe` WRITE;
/*!40000 ALTER TABLE `stufe` DISABLE KEYS */;
INSERT INTO `stufe` VALUES (1,'5'),(2,'6'),(3,'7'),(4,'8'),(5,'8'),(6,'9'),(7,'10'),(8,'11'),(9,'12');
/*!40000 ALTER TABLE `stufe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stunde`
--

DROP TABLE IF EXISTS `stunde`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stunde` (
  `idstunde` int(11) NOT NULL AUTO_INCREMENT,
  `startzeit` time NOT NULL,
  `endzeit` time NOT NULL,
  `bezeichnung` int(11) NOT NULL,
  PRIMARY KEY (`idstunde`),
  UNIQUE KEY `Bezeichnung_UNIQUE` (`bezeichnung`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stunde`
--

LOCK TABLES `stunde` WRITE;
/*!40000 ALTER TABLE `stunde` DISABLE KEYS */;
INSERT INTO `stunde` VALUES (1,'08:00:00','08:45:00',1),(2,'08:45:00','09:30:00',2);
/*!40000 ALTER TABLE `stunde` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stundenplanstunde`
--

DROP TABLE IF EXISTS `stundenplanstunde`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stundenplanstunde` (
  `idstundenplaneinheit` int(11) NOT NULL AUTO_INCREMENT,
  `raum_fk` int(11) DEFAULT NULL,
  `unterrichtseinheit_fk` int(11) DEFAULT NULL,
  `tag` tinyint(4) NOT NULL,
  `stunde_fk` int(11) NOT NULL,
  PRIMARY KEY (`idstundenplaneinheit`),
  KEY `stundenplanstunde_stunde_fk_idx` (`stunde_fk`),
  CONSTRAINT `stundenplanstunde_stunde_fk` FOREIGN KEY (`stunde_fk`) REFERENCES `stunde` (`idstunde`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stundenplanstunde`
--

LOCK TABLES `stundenplanstunde` WRITE;
/*!40000 ALTER TABLE `stundenplanstunde` DISABLE KEYS */;
INSERT INTO `stundenplanstunde` VALUES (3,1,46,2,1);
/*!40000 ALTER TABLE `stundenplanstunde` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unterrichtseinheit`
--

DROP TABLE IF EXISTS `unterrichtseinheit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unterrichtseinheit` (
  `idunterrichtseinheit` int(11) NOT NULL AUTO_INCREMENT,
  `is_klasse` tinyint(1) NOT NULL,
  `fach_fk` int(11) NOT NULL,
  PRIMARY KEY (`idunterrichtseinheit`),
  KEY `unterrichtseinheit_fach_fk_idx` (`fach_fk`),
  CONSTRAINT `unterrichtseinheit_fach_fk` FOREIGN KEY (`fach_fk`) REFERENCES `fach` (`idfach`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unterrichtseinheit`
--

LOCK TABLES `unterrichtseinheit` WRITE;
/*!40000 ALTER TABLE `unterrichtseinheit` DISABLE KEYS */;
INSERT INTO `unterrichtseinheit` VALUES (46,1,1),(47,1,2),(48,0,1),(49,1,3);
/*!40000 ALTER TABLE `unterrichtseinheit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unterrichtsstoff`
--

DROP TABLE IF EXISTS `unterrichtsstoff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unterrichtsstoff` (
  `idunterrichtsstoff` int(11) NOT NULL AUTO_INCREMENT,
  `idklasenbucheintrag` int(11) NOT NULL,
  `unterrichtsgegenstand` text,
  PRIMARY KEY (`idunterrichtsstoff`,`idklasenbucheintrag`),
  KEY `stoff_klassenbucheintrag_fk_idx` (`idklasenbucheintrag`),
  CONSTRAINT `stoff_klassenbucheintrag_fk` FOREIGN KEY (`idklasenbucheintrag`) REFERENCES `klassenbucheintrag` (`idklassenbucheintrag`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unterrichtsstoff`
--

LOCK TABLES `unterrichtsstoff` WRITE;
/*!40000 ALTER TABLE `unterrichtsstoff` DISABLE KEYS */;
/*!40000 ALTER TABLE `unterrichtsstoff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verspaetung`
--

DROP TABLE IF EXISTS `verspaetung`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verspaetung` (
  `idverspaetung` int(11) NOT NULL AUTO_INCREMENT,
  `idklassenbucheintrag_fk` int(11) NOT NULL,
  PRIMARY KEY (`idverspaetung`,`idklassenbucheintrag_fk`),
  KEY `verspaetung_klassenbucheintrag_fk_idx` (`idklassenbucheintrag_fk`),
  CONSTRAINT `verspaetung_klassenbucheintrag_fk` FOREIGN KEY (`idklassenbucheintrag_fk`) REFERENCES `klassenbucheintrag` (`idklassenbucheintrag`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verspaetung`
--

LOCK TABLES `verspaetung` WRITE;
/*!40000 ALTER TABLE `verspaetung` DISABLE KEYS */;
/*!40000 ALTER TABLE `verspaetung` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-26  3:05:26