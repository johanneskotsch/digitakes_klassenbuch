package MySQL;

import java.io.Closeable;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
public class DBController implements Serializable, AutoCloseable {

	private DBInterface dbInterface;

	@PostConstruct
	public void init() {
		initDBI();	
	}

	public DBController initDBI() {
		dbInterface = new DBInterface();
		return this;
	}

	/*
	 * @Inject public DBController(DBInterface dBInterface) { this.dBInterface =
	 * dBInterface; }
	 */

	public Serializable save(Object object) {
		return dbInterface.save(object);
	}

	public <T> T get(Class clazz, Long id) {
		return dbInterface.get(clazz, id);
	}

	public <T> T get(Class clazz, int id) {
		return dbInterface.get(clazz, new Integer(id));
	}

	public boolean login(String username, String password) {
		return true;
	}

	public <T> List<T> getAll(Class clazz) {
		return dbInterface.getAll(clazz);
	}

	public void close() {
		dbInterface.close();
	}

	public void deleteEntireTableContent(String tablename) {
		dbInterface.deleteEntireTableContent(tablename);
	}

}
