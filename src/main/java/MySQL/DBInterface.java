package MySQL;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class DBInterface implements AutoCloseable {// , DBInterfaceInterface{

	SessionFactory sessionFactory;
	Session session;

	public DBInterface() {
		SessionFactoryCreator sessionFactoryCreator = new SessionFactoryCreator();
		sessionFactory = sessionFactoryCreator.getSessionFactory();
		session = sessionFactory.openSession();
	}

	public Serializable save(Object object) {
		session.beginTransaction();
		System.out.println(object);
		Serializable returnValue = session.save(object);
		session.getTransaction().commit();
		return returnValue;
	}

	public void deleteEntireTableContent(String tablename) {
		session.beginTransaction();
		String query = "delete from " + tablename;
		Query q = session.createSQLQuery(query);
		q.executeUpdate();
		session.getTransaction().commit();
	}

	public <T> T get(Class clazz, Long id) {
		session.beginTransaction();
		Object returnValue = session.get(clazz, id);
		session.getTransaction().commit();
		return (T) returnValue;
	}

	public <T> T get(Class clazz, int id) {
		return get(clazz, (int) id);
	}

	public <T> List<T> getAll(Class clazz) {
		List<T> rList = new ArrayList<T>();
		List<T> dbList = session.createCriteria(clazz).list();
		for (T t: dbList) {
			rList.add(t);
		}
		return rList;

	}

	public boolean login(String kuerzel, String pwd) {
		System.out.println("login");
		return true; // stub
	}

	public void close() {
		session.close();
	}

	public void remove(Object t) {
		System.out.println("delete " + t);
		session.beginTransaction();
		session.delete(t);
		session.getTransaction().commit();
	}
}
