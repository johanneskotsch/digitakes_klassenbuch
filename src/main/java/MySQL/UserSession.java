package MySQL;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Specializes;
import javax.inject.Named;

import org.omnifaces.util.Faces;

import com.github.adminfaces.template.session.AdminSession;

import MySQL.model.Klasse;
import MySQL.model.Unterrichtseinheit;
import MySQL.model.User;

@Named
@SessionScoped
@Specializes
public class UserSession extends AdminSession implements Serializable {

	private User user;
	private String currentUser;
	private String kuerzel;
	private String password;
	private boolean remember;
	private int iduser;
	private DBInterface dbInterface;
	private String layout;
	private String typ;
	private List<Klasse> klasseList;

	@PostConstruct
	public void init() {
		dbInterface = new DBInterface();
	}

	public void login() {

		
		try {
			if (dbInterface.login(kuerzel, password)) {
				currentUser = kuerzel;
				
				List<Unterrichtseinheit> uhList = new ArrayList<Unterrichtseinheit>();

				//	this.user = new User(1, "qq", null, null, null, null, 1, null, null, );
				// iduser bestimmen
				// nach typ unterscheiden
				typ = "admin"; // example
				if(kuerzel.equals("q")) {
					typ = "lehrer";
				}
				setDefaultLayout();

				switch (typ) {
				case "lehrer":
					currentUser = kuerzel;
					System.out.println("lehrer");
					Faces.redirect("index.xhtml");
					break;

				case "admin":
					break;

				default:
					Faces.redirect("login.xhtml"); // login again
					break;
				}

			} else {
				Faces.redirect("login.xhtml"); // login again
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean isLoggedIn() {
		return currentUser != null;
	}

	public String getKuerzel() {
		return kuerzel;
	}

	public void setKuerzel(String email) {
		this.kuerzel = email;
	}

	public void setHorizontalLayout() {
		switch (typ) {
		case "lehrer":
			
			break;

		case "admin":
			layout = "/WEB-INF/templates/template-horizontal.xhtml";
			break;

		default:
			break;
		}
	}

	public void setDefaultLayout() {
		switch (typ) {
		case "lehrer":
			layout = "/WEB-INF/templates/template-lehrer.xhtml";
			break;

		case "admin":
			layout = "/WEB-INF/templates/template.xhtml";
			break;

		default:
			break;
		}
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isRemember() {
		return remember;
	}

	public void setRemember(boolean remember) {
		this.remember = remember;
	}

	public String getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}

	public int getIduser() {
		return iduser;
	}

	public void setIduser(int idlehrer) {
		this.iduser = idlehrer;
	}

	public String getLayout() {
		return layout;
	}

	public User getUser() {
		return user;
	}

	public List<Klasse> getKlasseList() {
		/*
		List<Klasse> klasseList = new ArrayList<Klasse>();
		klasseList.add(new Klasse( 1, "s1"));
		klasseList.add(new Klasse( 2, "s2"));

		System.out.println(klasseList);
		return klasseList;
		*/
		return StatDB.dbi.getAll(Klasse.class);
	}

}
