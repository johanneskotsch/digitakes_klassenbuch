package MySQL.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity(name = "fach")
public class Fach {
	@Id
	@GeneratedValue
	@Column(name = "idfach")
	Long id;
	@ManyToMany(mappedBy = "faecher")
	List<User> users;
	@Column(name = "bezeichnung")
	String bezeichnung;
	@Column(name = "kuerzel")
	String kuerzel;

	public Fach() {
	}

	public Fach(String bezeichnung, String kuerzel) {
		this.users = users;
		this.bezeichnung = bezeichnung;
		this.kuerzel = kuerzel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long idfach) {
		this.id = idfach;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public String getKuerzel() {
		return kuerzel;
	}

	public void setKuerzel(String kuerzel) {
		this.kuerzel = kuerzel;
	}

	@Override
	public String toString() {
		return "Fach [idfach=" + id + ", users=" + users + ", bezeichnung=" + bezeichnung + ", kuerzel=" + kuerzel
				+ "]";
	}

}
