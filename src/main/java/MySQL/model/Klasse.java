package MySQL.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Klasse {

	@Id
	@GeneratedValue
	@Column(name = "idklasse")
	long idklasse;
	@Column(name = "stufe")
	int stufe;
	@Column(name = "bezeichnung")
	String bezeichnung;
	@OneToMany(mappedBy = "klasse")
	List<Schueler>schueler;
	
	
	
	public Klasse(int stufe, String bezeichnung){
		this.stufe = stufe;
		this.bezeichnung = bezeichnung;
	}

	public Klasse() {
		
	}
	
	public long getIdklasse() {
		return idklasse;
	}
	public void setIdklasse(long idklasse) {
		this.idklasse = idklasse;
	}
	public int getStufe() {
		return stufe;
	}
	public void setStufe(int stufe) {
		this.stufe = stufe;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

    public List<Schueler> getSchueler() {
        return schueler;
    }

    public void setSchueler(List<Schueler> schueler) {
        this.schueler = schueler;
    }
	
	
	
}
