package MySQL.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "klassenbucheintrag")
public class Klassenbucheintrag {
    @Column(name = "eintrag")
    String eintrag;
    @Id
    @GeneratedValue
    @Column(name = "idklassenbucheintrag")
    long idklassenbucheintrag;
    @ManyToOne
    @JoinColumn(name = "schueler_fk")
    Schueler schueler;
    @Column(name = "datum")
    Date datum;
    public Klassenbucheintrag(){
    }
    public Klassenbucheintrag(String eintrag, Schueler schueler,Date datum) {
        this.eintrag = eintrag;
        this.schueler = schueler;
        this.datum=datum;
    }
    public String getEintrag() {
        return eintrag;
    }
    public void setEintrag(String eintrag) {
        this.eintrag = eintrag;
    }
    public long getIdklassenbucheintrag() {
        return idklassenbucheintrag;
    }
    public void setIdklassenbucheintrag(long idklassenbucheintrag) {
        this.idklassenbucheintrag = idklassenbucheintrag;
    }
    public Schueler getSchueler() {
        return schueler;
    }
    public void setSchueler(Schueler schueler) {
        this.schueler = schueler;
    }
    public Date getDatum() {
        return datum;
    }
    public void setDatum(Date datum) {
        this.datum = datum;
    }
    //e
}
