package MySQL.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity(name = "Kurs")
public class Kurs {
	
	@Id
	@GeneratedValue
	@Column(name = "idkurs")
	int idkurs;
	@Column(name = "bezeichnung")
	String bezeichnung;
	@Column(name = "unterrichtseinheit_fk")
	int unterrichtseinheit_fk;
	
	
/*	 @ManyToMany
	 @JoinTable(name = "kurs_stufen",
	                joinColumns=@JoinColumn(name="idkurs_fk",referencedColumnName="idkurs"),
	                inverseJoinColumns=@JoinColumn(name="idstufe_fk", referencedColumnName="idstufe")
	                )
	    List<Kurs> kurse;
*/

	 public Kurs() {
		 
	 }
	 
	public Kurs(int idkurs, String bezeichnung, int unterrichtseinheit_fk, List<Kurs> kurse) {
		super();
		this.idkurs = idkurs;
		this.bezeichnung = bezeichnung;
		this.unterrichtseinheit_fk = unterrichtseinheit_fk;
//		this.kurse = kurse;
	}


	public int getIdkurs() {
		return idkurs;
	}


	public void setIdkurs(int idkurs) {
		this.idkurs = idkurs;
	}


	public String getBezeichnung() {
		return bezeichnung;
	}


	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}


	public int getUnterrichtseinheit_fk() {
		return unterrichtseinheit_fk;
	}


	public void setUnterrichtseinheit_fk(int unterrichtseinheit_fk) {
		this.unterrichtseinheit_fk = unterrichtseinheit_fk;
	}


/*	public List<Kurs> getKurse() {
		return kurse;
	}


	public void setKurse(List<Kurs> kurse) {
		this.kurse = kurse;
	}*/

	 
	 
}
