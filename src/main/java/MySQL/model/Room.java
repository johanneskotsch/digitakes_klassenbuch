package MySQL.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity(name = "RAUM")
public class Room implements Serializable{
    @Id
    @GeneratedValue
    @Column(name="idraum")
    private Long id;
    @Column(name="bezeichnung")
    private String bezeichnung;
    
    public Room(){
        
    }
    
    public Room(String name) {
        this.bezeichnung = name;
    }
    
    public Room(Long id, String bezeichnung) {
    	this.id =  id;
    	this.bezeichnung = bezeichnung;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id =  id;
    }
    public String getBezeichnung() {
        return bezeichnung;
    }
    public void setBezeichnung(String firstName) {
        this.bezeichnung = firstName;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bezeichnung == null) ? 0 : bezeichnung.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Room other = (Room) obj;
		if (bezeichnung == null) {
			if (other.bezeichnung != null)
				return false;
		} else if (!bezeichnung.equals(other.bezeichnung))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Room [id=" + id + ", bezeichnung=" + bezeichnung + "]";
	}
    
    
    
}
