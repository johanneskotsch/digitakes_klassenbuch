package MySQL.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name = "Schueler")
public class Schueler {
	@Id
	@GeneratedValue
	@Column(name = "idschueler")
	long idschueler;
	@Column(name = "interne_verwaltungsnummer")
	int interne_verwaltungsnummer;
	@Column(name = "vorname")
	String vorname;
	@Column(name = "nachname")
	String nachname;
	@Column(name = "geburtsdatum")
	Date geburtsdatum;
	@OneToMany(mappedBy = "schueler")
	List<Klassenbucheintrag>klassenbucheintraege;
	
	 @ManyToMany
	 @JoinTable(name = "schueler_besucht_kurs",
	                joinColumns=@JoinColumn(name="schueler_fk",referencedColumnName="idschueler"),
	                inverseJoinColumns=@JoinColumn(name="kurs_fk", referencedColumnName="idkurs")
	                )
	    List<Kurs> kurse;
	
	 @ManyToOne
	 @JoinColumn(name="klasse_fk")
	 Klasse klasse;
	public Schueler() {
		
	}
	
	public Schueler( int  interne_verwaltungsnummer, String vorname, String nachname, Date geburtsdatum) {
		this.interne_verwaltungsnummer = interne_verwaltungsnummer;
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsdatum = geburtsdatum;		
	}

	public long getIdschueler() {
		return idschueler;
	}

	public void setIdschueler(long idschueler) {
		this.idschueler = idschueler;
	}

	public int getInterne_verwaltungsnummer() {
		return interne_verwaltungsnummer;
	}

	public void setInterne_verwaltungsnummer(int interne_verwaltungsnummer) {
		this.interne_verwaltungsnummer = interne_verwaltungsnummer;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public Date getGeburtsdatum() {
		return geburtsdatum;
	}

	public void setGeburtsdatum(Date geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}

	public Klasse getKlasse() {
		return klasse;
	}

	public void setKlasse(Klasse klasse) {
		this.klasse = klasse;
	}
	
	public List<Kurs> getKurse() {
		return kurse;
	}

	public void setKurse(List<Kurs> kurse) {
		this.kurse = kurse;
	}

    public List<Klassenbucheintrag> getKlassenbucheintraege() {
        return klassenbucheintraege;
    }

    public void setKlassenbucheintraege(List<Klassenbucheintrag> klassenbucheintraege) {
        this.klassenbucheintraege = klassenbucheintraege;
    }
//e
	
}
