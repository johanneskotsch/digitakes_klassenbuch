package MySQL.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Stufe {

    @Id
    @GeneratedValue
    @Column(name = "idstufe")
    int idstufe;
    @Column(name = "bezeichnung")
    String bezeichnung;
    
    
    
    public Stufe (){
        
    }
    
    
    public Stufe(int idstufe, String bezeichnung) {
        super();
        this.idstufe = idstufe;
        this.bezeichnung = bezeichnung;
    }


    public int getIdstufe() {
        return idstufe;
    }
    public void setIdstufe(int idstufe) {
        this.idstufe = idstufe;
    }
    public String getBezeichnung() {
        return bezeichnung;
    }
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
 
    
}
