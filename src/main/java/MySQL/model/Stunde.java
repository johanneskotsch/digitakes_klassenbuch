package MySQL.model;

import java.sql.Time;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity(name = "stunde")
public class Stunde {
    @Id
    @GeneratedValue
    @Column(name="idstunde")
    int idstunde;
    @Column(name = "startzeit")
    Time startzeit;
    @Column(name = "endzeit")
    Time endzeit;
    @Column(name = "bezeichnung")
    int bezeichnung;
    
    
    public Stunde(){
    }
    
    public Stunde(Time startzeit, Time endzeit, int bezeichnung) {
        this.startzeit = startzeit;
        this.endzeit = endzeit;
        this.bezeichnung = bezeichnung;
    }
    public int getIdstunde() {
        return idstunde;
    }
    public void setIdstunde(int idstunde) {
        this.idstunde = idstunde;
    }
    public Time getStartzeit() {
        return startzeit;
    }
    public void setStartzeit(Time startzeit) {
        this.startzeit = startzeit;
    }
    public Time getEndzeit() {
        return endzeit;
    }
    public void setEndzeit(Time endzeit) {
        this.endzeit = endzeit;
    }
    public int getBezeichnung() {
        return bezeichnung;
    }
    public void setBezeichnung(int bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
    
}
