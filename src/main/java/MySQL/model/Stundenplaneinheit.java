package MySQL.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity(name = "stundenplaneinheit")
public class Stundenplaneinheit {
    @Id
    @GeneratedValue
    @Column(name="idstundenplaneinheit")
    int idstundenplaneinheit;
    @ManyToOne
    @JoinColumn(name = "raum_fk")
    Room room;
    /*@ManyToMany
    @JoinTable(name = "stundenplaneinheit_unterrichtseinheit",
                joinColumns=@JoinColumn(name="stundenplaneinheit_fk",referencedColumnName="idstundenplaneinheit"),
                inverseJoinColumns=@JoinColumn(name="unterrichtseinheit_fk", referencedColumnName="idunterrichtseinheit")
                )
    List<Unterrichtseinheit> unterrichtseinheiten;
    */@Column(name= "tag")
    int tag;
    @ManyToOne
    @JoinColumn(name = "stunde_fk")
    Stunde stunde;
    
    public Stundenplaneinheit(){
    }
    
    public Stundenplaneinheit(int tag) {
        this.tag = tag;
    }

    public int getIdstundenplaneinheit() {
        return idstundenplaneinheit;
    }

    public void setIdstundenplaneinheit(int idstundenplaneinheit) {
        this.idstundenplaneinheit = idstundenplaneinheit;
    }

   
/*
    public List<Unterrichtseinheit> getUnterrichtseinheit() {
        return unterrichtseinheiten;
    }

    public void setUnterrichtseinheiten(List<Unterrichtseinheit> unterrichtseinheiten) {
        this.unterrichtseinheiten = unterrichtseinheiten;
    }
*/
    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public Stunde getStunde() {
        return stunde;
    }

    public void setStunde(Stunde stunde) {
        this.stunde = stunde;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
    
    
}
