package MySQL.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity(name = "unterrichtseinheit")
public class Unterrichtseinheit {
    @Id
    @GeneratedValue
    @Column(name="idunterrichtseinheit")
    int idunterrichtseinheit;
    @Column(name = "is_klasse")
    int is_klasse;
    @Column(name = "fach_fk")
    int fach_fk;
    @ManyToMany(mappedBy = "unterrichtseinheiten")
    List<User>lehrer;
  //  @ManyToMany(mappedBy = "unterrichtseinheiten")
    //List<Stundenplaneinheit>stundenplaneinheiten;
    
    public Unterrichtseinheit(){
    }
    
    public Unterrichtseinheit(int is_klasse, int fach_fk) {
        this.is_klasse = is_klasse;
        this.fach_fk = fach_fk;
    }
    public int getIdunterrichtseinheit() {
        return idunterrichtseinheit;
    }
    public void setIdunterrichtseinheit(int idunterrichtseinheit) {
        this.idunterrichtseinheit = idunterrichtseinheit;
    }
    public int getIs_klasse() {
        return is_klasse;
    }
    public void setIs_klasse(int is_klasse) {
        this.is_klasse = is_klasse;
    }
    public int getFach_fk() {
        return fach_fk;
    }
    public void setFach_fk(int fach_fk) {
        this.fach_fk = fach_fk;
    }
    public List<User> getLehrer() {
        return lehrer;
    }
    public void setLehrer(List<User> lehrer) {
        this.lehrer = lehrer;
    }

    /*
    public List<Stundenplaneinheit> getStundenplaneinheiten() {
        return stundenplaneinheiten;
    }

    public void setStundenplaneinheiten(List<Stundenplaneinheit> stundenplaneinheiten) {
        this.stundenplaneinheiten = stundenplaneinheiten;
    }

	@Override
	public String toString() {
		return "Unterrichtseinheit [idunterrichtseinheit=" + idunterrichtseinheit + ", is_klasse=" + is_klasse
				+ ", fach_fk=" + fach_fk + ", lehrer=" + lehrer + ", stundenplaneinheiten=" + stundenplaneinheiten
				+ "]";
	}
	*/   
}