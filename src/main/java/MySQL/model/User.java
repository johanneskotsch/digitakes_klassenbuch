package MySQL.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
@Entity(name = "User")
public class User {
    @Id
    @GeneratedValue
    @Column(name="idlehrer")
    Long userId;
    @Column(name = "kuerzel")
    String kuerzel;
    @Column(name = "vorname")
    String vorname;
    @Column(name = "nachname")
    String nachname;
    @Column(name = "geburtsdatum")
    Date geburtsdatum;
    @Column(name = "typ")
    String typ;
    @Column(name = "interne_verwaltungsnummer")
    int interne_verwaltungsnummer;
    @Column(name = "passwort")
    String passwort;
    @ManyToMany
    @JoinTable(name = "user_unterrichtet_fach",
                joinColumns=@JoinColumn(name="idlehrer",referencedColumnName="idlehrer"),
                inverseJoinColumns=@JoinColumn(name="idfach", referencedColumnName="idfach")
                )
    List<Fach> faecher;
    @ManyToMany
    @JoinTable(name = "user_unterrichtet_unterrichtseinheit",
    joinColumns=@JoinColumn(name="lehrer_fk",referencedColumnName="idlehrer"),
    inverseJoinColumns=@JoinColumn(name="unterrichtseinheit_fk", referencedColumnName="idunterrichtseinheit")
    )
    List<Unterrichtseinheit> unterrichtseinheiten;
    public User(){        
    }
    
    public User(String kuerzel, String vorname, String nachname, Date geburtsdatum, String typ,
            int interne_verwaltungsnummer, String passwort) {
        this.kuerzel = kuerzel;
        this.vorname = vorname;
        this.nachname = nachname;
        this.geburtsdatum = geburtsdatum;
        this.typ = typ;
        this.interne_verwaltungsnummer = interne_verwaltungsnummer;
        this.passwort = passwort;
    }
    
    
    
    public User(Long userId, String kuerzel, String vorname, String nachname, Date geburtsdatum, String typ,
			int interne_verwaltungsnummer, String passwort, List<Fach> faecher,
			List<Unterrichtseinheit> unterrichtseinheiten) {
		this.userId = userId;
		this.kuerzel = kuerzel;
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsdatum = geburtsdatum;
		this.typ = typ;
		this.interne_verwaltungsnummer = interne_verwaltungsnummer;
		this.passwort = passwort;
		this.faecher = faecher;
		this.unterrichtseinheiten = unterrichtseinheiten;
	}

	public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public String getKuerzel() {
        return kuerzel;
    }
    public void setKuerzel(String kuerzel) {
        this.kuerzel = kuerzel;
    }
    public String getVorname() {
        return vorname;
    }
    public void setVorname(String vorname) {
        this.vorname = vorname;
    }
    public String getNachname() {
        return nachname;
    }
    public void setNachname(String nachname) {
        this.nachname = nachname;
    }
    public Date getGeburtsdatum() {
        return geburtsdatum;
    }
    public void setGeburtsdatum(Date geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }
    public String getTyp() {
        return typ;
    }
    public void setTyp(String typ) {
        this.typ = typ;
    }
    public int getInterne_verwaltungsnummer() {
        return interne_verwaltungsnummer;
    }
    public void setInterne_verwaltungsnummer(int interne_verwaltungsnummer) {
        this.interne_verwaltungsnummer = interne_verwaltungsnummer;
    }
    public String getPasswort() {
        return passwort;
    }
    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public List<Fach> getFaecher() {
        return faecher;
    }

    public void setFaecher(List<Fach> faecher) {
        this.faecher = faecher;
    }

    public List<Unterrichtseinheit> getUnterrichtseinheiten() {
        return unterrichtseinheiten;
    }

    public void setUnterrichtseinheiten(List<Unterrichtseinheit> unterrichtseinheiten) {
        this.unterrichtseinheiten = unterrichtseinheiten;
    }

	@Override
	public String toString() {
		return "User [userId=" + userId + ", kuerzel=" + kuerzel + ", vorname=" + vorname + ", nachname=" + nachname
				+ ", geburtsdatum=" + geburtsdatum + ", typ=" + typ + ", interne_verwaltungsnummer="
				+ interne_verwaltungsnummer + ", passwort=" + passwort + ", faecher=" + faecher
				+ ", unterrichtseinheiten=" + unterrichtseinheiten + "]";
	}
    
    
    
    
}
