package MySQL.testPackage;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import MySQL.DBController;
import MySQL.DBInterface;
import MySQL.model.Fach;
import MySQL.model.Klasse;
import MySQL.model.Klassenbucheintrag;
import MySQL.model.Room;
import MySQL.model.Schueler;
import MySQL.model.Stunde;
import MySQL.model.Stundenplaneinheit;
import MySQL.model.Unterrichtseinheit;
import MySQL.model.User;

public class DBTest {
    static List<Fach> faecher;
    static List<Unterrichtseinheit> unterrichtseinheiten;
    static List<Stundenplaneinheit> stundenplaneinheiten;
    static List<Room> raeume;
    static List<Stunde> stunden;
    static List<Schueler>schueler;
    static List<Klasse>klassen;
    static List<Klassenbucheintrag>klassenbucheintraege;
    private static DBController dBController;

    @Before
    public void setup() {
        dBController = new DBController();
        dBController.init();
        dBController.deleteEntireTableContent("user_unterrichtet_fach");
        dBController.deleteEntireTableContent("user_unterrichtet_unterrichtseinheit");
        dBController.deleteEntireTableContent("klasse_ist_unterrichtseinheit");
        dBController.deleteEntireTableContent("kurs_stufen");
        dBController.deleteEntireTableContent("user_ist_klassenlehrer");
        dBController.deleteEntireTableContent("stundenplaneinheit_unterrichtseinheit");
        dBController.deleteEntireTableContent("kurs");
        dBController.deleteEntireTableContent("unterrichtseinheit");
        dBController.deleteEntireTableContent("fach");
        dBController.deleteEntireTableContent("User");
        dBController.deleteEntireTableContent("stundenplaneinheit");
        dBController.deleteEntireTableContent("stunde");
        dBController.deleteEntireTableContent("raum");
        dBController.deleteEntireTableContent("schueler_ist_in_klasse");
        dBController.deleteEntireTableContent("schueler");
        dBController.deleteEntireTableContent("klasse");
        dBController.deleteEntireTableContent("klassenbucheintrag");
        faecher = new ArrayList<Fach>();
        unterrichtseinheiten = new ArrayList<Unterrichtseinheit>();
        stundenplaneinheiten = new ArrayList<Stundenplaneinheit>();
        raeume = new ArrayList<Room>();
        stunden = new ArrayList<Stunde>();
        schueler = new ArrayList<Schueler>();
        klassenbucheintraege = new ArrayList<Klassenbucheintrag>();
        createObjects();
        for (int i = 0; i < faecher.size(); i++) {
            dBController.save(faecher.get(i));
        }
    }


    // @Test
    public void simpleSelectRoom1() {
        assertEquals("r1", dBController.<Room>get(Room.class, 1).getBezeichnung());
        System.out.println(dBController.getAll(Room.class));
    }

//     @Test
    public void addUserAndFach() {
        User user = new User("khlb", "bernd", "kahlbrandt", new Date(1939, 9, 1), "Lehrer", 1939, "hh");
        user.setFaecher(faecher);
        dBController.save(user);
        List<User> list = dBController.getAll(User.class);
        assertEquals(list.get(0), user);
    }

//     @Test
    public void TestUserUnterrichtseinheitFaecher() {
        for (int i = 0; i < faecher.size(); i++) {
           // unterrichtseinheiten.add(new Unterrichtseinheit(1, faecher.get(i).getId())); LONG INT PROBLEM
            dBController.save(unterrichtseinheiten.get(i));
        }

        User user = new User("khlb", "bernd", "kahlbrandt", new Date(1939, 9, 1), "Lehrer", 1939, "hh");
        user.setFaecher(faecher);
        user.setUnterrichtseinheiten(unterrichtseinheiten);
        dBController.save(user);
        List<User> list = dBController.getAll(User.class);
        assertEquals(list.get(0), user);
    }

//    @Test
    public void testRoomStundenplaneinheitStunde() {
        for (int i = 0; i < stundenplaneinheiten.size(); i++) {
            dBController.save(raeume.get(i));
            stundenplaneinheiten.get(i).setRoom(raeume.get(0));
            dBController.save(stunden.get(i));
            stundenplaneinheiten.get(i).setStunde(stunden.get(i));
            System.out.println("-----------------------");
            dBController.save(stundenplaneinheiten.get(i));
        }
        List<Stundenplaneinheit> dbStundenplaneinheiten = dBController.getAll(Stundenplaneinheit.class);
        assertEquals(dbStundenplaneinheiten.get(0).getRoom(), stundenplaneinheiten.get(0).getRoom());
        assertEquals(dbStundenplaneinheiten.get(0).getStunde(),stundenplaneinheiten.get(0).getStunde());
    }
    
//    @Test
    public void testStundenplaneinheitUnterrichtseinheit(){
        List<Unterrichtseinheit>unterrichtseinheiten = new ArrayList<Unterrichtseinheit>();
        //unterrichtseinheiten.add(new Unterrichtseinheit(1, faecher.get(0).getId()));
        //unterrichtseinheiten.add(new Unterrichtseinheit(1, faecher.get(1).getId()));
        //unterrichtseinheiten.add(new Unterrichtseinheit(1, faecher.get(2).getId()));
        for(int i = 0; i < unterrichtseinheiten.size();i++){
            //unterrichtseinheiten.get(i).setStundenplaneinheiten(stundenplaneinheiten);
            dBController.save(unterrichtseinheiten.get(i));
        }
        for(int i = 0; i < stundenplaneinheiten.size();i++){
            //stundenplaneinheiten.get(i).setUnterrichtseinheiten(unterrichtseinheiten);
            dBController.save(stunden.get(i));
            stundenplaneinheiten.get(i).setStunde(stunden.get(i));
            dBController.save(stundenplaneinheiten.get(i));
        }
        
    }
    
//    @Test
    public void testSchuelerIstInKlasse(){
        Klasse k = new Klasse(1,"1a");
        k.setSchueler(schueler);
        dBController.save(k);
        for(Schueler s:schueler){
            dBController.save(s);
        }
        //immer die id neu reinschreiben, da man über .save() nicht die neue id bekommt
        Klasse k1 = dBController.get(Klasse.class,15);
        for(Schueler s:k1.getSchueler()){
            System.out.println(s.getNachname());
        }
        assertTrue(k1.getSchueler().size()==3);
//        assertEquals(list.get(0), k);
    }

    
    @Test
    public void testSchuelerKlassenbucheintrag(){
        Schueler s = new Schueler(1,"john","johnson",new Date(1990,9,1));
        Schueler s2 = new Schueler(2,"mert","sentuerk",new Date(1995,10,3));
        klassenbucheintraege.add(new Klassenbucheintrag("abszenz", s,new Date(1999,9,1)));
        klassenbucheintraege.add(new Klassenbucheintrag("zu spaet", s,new Date(1998,8,2)));
        klassenbucheintraege.add(new Klassenbucheintrag("wollte keine liebe", s,new Date(1997,7,3)));
        List<Klassenbucheintrag>list = new ArrayList<Klassenbucheintrag>();
        list.add(new Klassenbucheintrag("abszenz",s2,new Date(2018,7,4)));
        
        s.setKlassenbucheintraege(klassenbucheintraege);
        s2.setKlassenbucheintraege(list);
        dBController.save(s2);
        list.get(0).setSchueler(s2);
        dBController.save(list.get(0));
        Long id =  (Long) dBController.save(s);
        for(Klassenbucheintrag k:klassenbucheintraege){
            k.setSchueler(s);
            dBController.save(k);
        }
        //immer die id neu reinschreiben, da man über .save() nicht die neue id bekommt
        Schueler s1 = dBController.get(Schueler.class,id);
        for(Klassenbucheintrag k1 : s1.getKlassenbucheintraege()){
            System.out.println(k1.getEintrag());
        }
        assertTrue(s1.getKlassenbucheintraege().size()==3);
    }
    //e
    @AfterClass
    public static void close() {
        dBController.close();
    }

    public static void createObjects() {
        faecher.add(new Fach("Mathematik", "mat"));
        faecher.add(new Fach("Deutsch", "deu"));
        faecher.add(new Fach("Biologie", "bio"));
        raeume.add(new Room("bt734"));
        raeume.add(new Room("bt735"));
        raeume.add(new Room("bt736"));
        stundenplaneinheiten.add(new Stundenplaneinheit(3));
        stundenplaneinheiten.add(new Stundenplaneinheit(4));
        stundenplaneinheiten.add(new Stundenplaneinheit(5));
        stunden.add(new Stunde(new Time(System.currentTimeMillis()), new Time(System.currentTimeMillis()), 1));
        stunden.add(new Stunde(new Time(System.currentTimeMillis()), new Time(System.currentTimeMillis()), 2));
        stunden.add(new Stunde(new Time(System.currentTimeMillis()), new Time(System.currentTimeMillis()), 3));
        schueler.add(new Schueler(1,"john","johnson",new Date(1990,9,1)));
        schueler.add(new Schueler(2,"johny","johnsons",new Date(1991,9,1)));
        schueler.add(new Schueler(1,"johnys","johnsonsi",new Date(1992,9,1)));
    }
}
