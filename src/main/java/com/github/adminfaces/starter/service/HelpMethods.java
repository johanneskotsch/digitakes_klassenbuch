package com.github.adminfaces.starter.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;

public class HelpMethods {

	public static String[] stringEnumerationToArray(String s) {
		int length = s.length() - 1;
		if((s.charAt(length) == ',' || s.charAt(length) == ' ') && length != -1) {
			s = s.substring(0, length - 1);
		}
		return s.split(" *?, *+");
	}
	

	

	public static String getStringListFromResultSet(ResultSet result) {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		try {
			while(result.next()) {
				if(first) {
					first = false;
				} else {
					sb.append(", ");

				}
				sb.append(result.getObject(1));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public static String getWochentagByInt(int i) {
		switch(i) {
			case 1: return "Sonntag";
			case 2: return "Montag";
			case 3: return "Dienstag";
			case 4: return "Mittwoch";
			case 5: return "Donnerstag";
			case 6: return "Freitag";
			case 7: return "Samstag";
			default: return"?";
		}
	}
	
	/*
	public static String getWochentagkuerzelByInt(int i) {
		switch(i) {
			case 1: return "So";
			case 2: return "Mo";
			case 3: return "Di";
			case 4: return "Mi";
			case 5: return "Do";
			case 6: return "Fr";
			case 7: return "Sa";
			default: return"?";
		}
	}
	*/
	
	public static int getWochenTagNrByText(String wochentag) {
		
		int wochentagNr;
		if(wochentag.equals("montag") || wochentag.equals("mo") || wochentag.equals("1")) {
			wochentagNr = 2;
		} else if(wochentag.equals("dienstag") || wochentag.equals("die") || wochentag.equals("di") || wochentag.equals("2")) {
			wochentagNr = 3;
		} else if(wochentag.equals("mittwoch") || wochentag.equals("mi") || wochentag.equals("3")) {
			wochentagNr = 4;
		} else if(wochentag.equals("donnerstag") || wochentag.equals("do") || wochentag.equals("4")) {
			wochentagNr = 5;
		} else if(wochentag.equals("freitag") || wochentag.equals("fr") || wochentag.equals("5")) {
			wochentagNr = 6;
		} else if(wochentag.equals("samstag") || wochentag.equals("sa") || wochentag.equals("6")) {
			wochentagNr = 7;
		} else if(wochentag.equals("sonntag") || wochentag.equals("so") || wochentag.equals("7")) {
			wochentagNr = 1;
		} else {
			throw new IllegalArgumentException();
		} 
		return wochentagNr;
	}




	public static Date makeTime(Calendar date, Time time) {
		Calendar returnTime = (Calendar) date.clone();
		Calendar tCal = Calendar.getInstance();
		tCal.setTime(time);
		returnTime.set(Calendar.HOUR_OF_DAY, tCal.get(Calendar.HOUR_OF_DAY));
		returnTime.set(Calendar.MINUTE, tCal.get(Calendar.MINUTE));
		
		return returnTime.getTime();
	}
}
