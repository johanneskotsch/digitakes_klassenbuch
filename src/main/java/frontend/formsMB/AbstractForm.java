package frontend.formsMB;

import static com.github.adminfaces.starter.util.Utils.addDetailMessage;
import static com.github.adminfaces.template.util.Assert.has;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

import javax.annotation.PostConstruct;

import org.omnifaces.util.Faces;

import MySQL.StatDB;

/**
 * AbstractForm stellt abstrakt die Formularfunktionalitäten für alle Entitäten
 * bereit. Die Funktionalitäten werden in den Formularen in XHTML benötigt, um
 * ein Element zu modifizieren oder neu anzulegen.
 * 
 * @author johannes
 * @param <T>
 *            Generischer Typ über welchen das Formular läuft
 */
public abstract class AbstractForm<T> implements Serializable {

	private Long id;
	/**
	 * Generische Instanz des im Formular bearbeiteten Objekts.
	 */
	protected T instance;
	Class<?> c;

	@PostConstruct
	public abstract void init();

	/**
	 * Die Methode wird von den erbenden Beans beim Initialisieren aufgerufen. Ziel
	 * ist es, instance zu initalisieren.
	 * 
	 * @param c
	 *            Class-Klasse der Entität über welchen das Formular läuft
	 */
	public void sInit(Class<?> c) {
		try {
			this.c = c;

			if (Faces.isAjaxRequest()) {
				return;
			}
			if (has(id)) {
				instance = StatDB.dbi.get(c, id);
			} else {
				instance = (T) c.newInstance();
			}
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public T getInstance() {
		return instance;
	}

	public void setInstance(T t) {
		this.instance = t;
	}

	/**
	 * Löscht das im Formular befindliche Objekt aus der Datenbank
	 * 
	 * @throws IOException
	 */
	public void remove() throws IOException {
			StatDB.dbi.remove(instance);
		

		addDetailMessage("Car " /* teacher.getModel() */ + " removed successfully");
		Faces.getFlash().setKeepMessages(true);
		Faces.redirect("car-list.xhtml");

	}

	public void remove(List<Object> l) {
		System.out.println("jetzt löschen: " +l);
		for (Object t : l) {
			StatDB.dbi.remove(t);
		}

	}

	/*
	 * public void remove(List<Object> l) { try {
	 * StaticDatabase.db.objectListRemove(l); } catch (SQLException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } }
	 */

	public void save() throws IllegalArgumentException, IllegalAccessException {
		String msg = "";
		StatDB.dbi.save(instance);
		if (isNew()) {
			msg = "Erfolgreich hinzugefügt.";
		} else {
			msg = "Erfolgreich geändert.";
		}
		addDetailMessage(msg);
	}

	public void clear() {
		try {
			instance = (T) (c.newInstance());
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		id = null;
	}

	public boolean isNew() {
		Field[] fs = c.getDeclaredFields();
		Field f = fs[0];
		f.setAccessible(true);
		try {
			return instance == null || f.get(instance) == null;
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return instance == null;
	}

}
