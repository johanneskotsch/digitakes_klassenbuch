package frontend.formsMB;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

import org.omnifaces.util.Faces;

import MySQL.model.Fach;

import static com.github.adminfaces.template.util.Assert.has;

@ManagedBean(name="fachForm")
@ViewScoped
public class FachForm extends AbstractForm<Fach> implements Serializable {

	@PostConstruct
	@Override
	public void init() {
		super.sInit(Fach.class);
	}

	@Override
	public void save() throws IllegalArgumentException, IllegalAccessException {
		super.save();
		try {
			Faces.redirect("fachliste.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
