package frontend.formsMB;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Faces;

import MySQL.model.Room;

@ManagedBean(name="raumForm")
@ViewScoped
public class RaumForm extends AbstractForm<Room> implements Serializable {

	@PostConstruct
	@Override
	public void init() {
		super.sInit(Room.class);
	}
	
	@Override
	public void save() throws IllegalArgumentException, IllegalAccessException {
		super.save();
		try {
			Faces.redirect("raumliste.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
