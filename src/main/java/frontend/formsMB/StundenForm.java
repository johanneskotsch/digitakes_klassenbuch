package frontend.formsMB;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Faces;

import MySQL.model.Stunde;

@ManagedBean(name="stundenForm")
@ViewScoped
public class StundenForm extends AbstractForm<Stunde> implements Serializable {

	@PostConstruct
	@Override
	public void init() {
		super.sInit(Stunde.class);
	}
	
	@Override
	public void save() throws IllegalArgumentException, IllegalAccessException {
		super.save();
		try {
			Faces.redirect("stundenliste.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
