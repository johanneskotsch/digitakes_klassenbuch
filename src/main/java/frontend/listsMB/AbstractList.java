package frontend.listsMB;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import MySQL.DBInterface;
import MySQL.StatDB;
import MySQL.model.Room;

public abstract class AbstractList<T> implements Serializable {
	List<T> list;
	List<T> selectedList = new ArrayList<T>();
	List<Integer> idList = new ArrayList<>();



	public List<T> getList() {
		return list;
	}

	public List<T> getSelectedList() {
		System.out.println("slist: " + selectedList);
		return selectedList;
	}

	public void setSelectedList(List<T> selectedList) {
		System.out.println(selectedList);
		//this.selectedList = selectedList;
	}

	public int getSelectedListSize() {
		if (selectedList == null) {
			return 0;
		}
		return selectedList.size();
	}
	
	public void delete(Object o) {
		StatDB.dbi.remove(o);
	}

    public void onSelect(SelectEvent sE) {
    	selectedList.add((T) sE.getObject());
    	System.out.println("new sellist:" + selectedList);
    	System.out.println("new idlist: " + idList);
    	//StatDB.dbi.remove(sE.getObject());
    }
    
    public void onUnSelect(UnselectEvent sE) {
    	selectedList.remove(sE.getObject());
    }

	
}