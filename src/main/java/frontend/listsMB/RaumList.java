package frontend.listsMB;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

import MySQL.StatDB;
import MySQL.model.Room;

@Named
@SessionScoped
public class RaumList extends AbstractList<Room> implements Serializable {

	@PostConstruct
	public void init() {
		list = StatDB.dbi.getAll(Room.class);
	}

}