package frontend.listsMB;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

import MySQL.StatDB;
import MySQL.model.Room;
import MySQL.model.Schueler;

@Named
@SessionScoped
public class SchuelerList extends AbstractList<Schueler> implements Serializable {

	@PostConstruct
	public void init() {
		list = StatDB.dbi.getAll(Schueler.class);
	}

}