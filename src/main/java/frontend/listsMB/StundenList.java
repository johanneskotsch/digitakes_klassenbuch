package frontend.listsMB;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

import MySQL.StatDB;
import MySQL.model.Stunde;

@Named
@SessionScoped
public class StundenList extends AbstractList<Stunde> implements Serializable {

	/*
	 * @PostConstruct public void init() { sInit(Room.class); }
	 */

	@PostConstruct
	public void init() {
		list = StatDB.dbi.getAll(Stunde.class);
	}

}