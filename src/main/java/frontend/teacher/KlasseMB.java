package frontend.teacher;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

import org.primefaces.event.UnselectEvent;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import MySQL.StatDB;
import MySQL.model.Klasse;
import MySQL.model.Schueler;

@Named
@SessionScoped
public class KlasseMB implements Serializable {
	
	List<Schueler> classList;
	List<Schueler> selectedList = new ArrayList<Schueler>();
	List<Integer> idList = new ArrayList<>();
	Integer id;
	BarChartModel model;
	
	@PostConstruct
	public void init() {
		this.model= initBarModel();
	}
	
	

	
	
	
	public List<Schueler> getClassList(int id) {
		System.out.println(id);
		return StatDB.dbc.<Klasse>get(Klasse.class, new Long(id)).getSchueler();
	}

	public List<Schueler> getSelectedList() {
		System.out.println("slist: " + selectedList);
		return selectedList;
	}
	
	private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();
 
        ChartSeries boys = new ChartSeries();
        boys.setLabel("Verspätungen");
        boys.set("Januar", 60);
        boys.set("Februar", 70);
        boys.set("März", 67);
        boys.set("April", 58);
        boys.set("Mai", 72);

 
        ChartSeries girls = new ChartSeries();
        girls.setLabel("Abesnzen");
        girls.set("Januar", 75);
        girls.set("Februar", 72);
        girls.set("März", 67);
        girls.set("April", 60);
        girls.set("Mai", 87);

 
        model.addSeries(boys);
        model.addSeries(girls);
         
        return model;
    }
	
	public BarChartModel getModel() {
		return initBarModel();
	}
	
	

	public void setSelectedList(List<Schueler> selectedList) {
		System.out.println(selectedList);
		//this.selectedList = selectedList;
	}

	public int getSelectedListSize() {
		if (selectedList == null) {
			return 0;
		}
		return selectedList.size();
	}
	
	public void delete(Object o) {
		StatDB.dbi.remove(o);
	}

	/*
    public void onSelect(SelectEvent sE) {
    	selectedList.add((T) sE.getObject());
    	System.out.println("new sellist:" + selectedList);
    	System.out.println("new idlist: " + idList);
    	//StatDB.dbi.remove(sE.getObject());
    }
    */
    
    public void onUnSelect(UnselectEvent sE) {
    	selectedList.remove(sE.getObject());
    }
	

}