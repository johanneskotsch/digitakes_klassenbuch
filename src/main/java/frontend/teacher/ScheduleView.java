package frontend.teacher;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Faces;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import MySQL.UserSession;
import MySQL.model.Stundenplaneinheit;
import MySQL.model.Unterrichtseinheit;
import frontend.teacher.model.concreteStunde;

@ManagedBean
@ViewScoped
public class ScheduleView implements Serializable {

	private ScheduleModel meinUnterrichtSchedule;

	@PostConstruct
	public void init() {
		meinUnterrichtSchedule = new DefaultScheduleModel();
		meinUnterrichtSchedule.addEvent(new DefaultScheduleEvent("Mathematik, 5a", nextDay9Am(), nextDay11Am(),
				new concreteStunde(new Unterrichtseinheit(1, 175), new Stundenplaneinheit(1))));

	}

	private Calendar today() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);

		return calendar;
	}

	private Date today1Pm() {
		Calendar t = (Calendar) today().clone();
		t.set(Calendar.AM_PM, Calendar.PM);
		t.set(Calendar.HOUR, 1);

		return t.getTime();
	}

	private Date today6Pm() {
		Calendar t = (Calendar) today().clone();
		t.set(Calendar.AM_PM, Calendar.PM);
		t.set(Calendar.HOUR, 3);

		return t.getTime();
	}

	public ScheduleModel getMeinUnterrichtSchedule(UserSession uSession) {
		if (meinUnterrichtSchedule == null) {

			System.out.println("Unterrichtseinheiten: " + uSession.getUser().getUnterrichtseinheiten());

			meinUnterrichtSchedule = new LazyScheduleModel() {
				@Override
				public void loadEvents(Date start, Date end) {
					long cTime = new Date().getTime();
					Unterrichtseinheit unterrichtseinheit = new Unterrichtseinheit();
					Stundenplaneinheit stundenplaneinheit = new Stundenplaneinheit();

					addEvent(new DefaultScheduleEvent("test", new Date(1529843542), new Date(1529843542 + 3600000),
							new concreteStunde(unterrichtseinheit, stundenplaneinheit)));

					Calendar cStart = Calendar.getInstance();
					cStart.setTime(start);
					Calendar cEnd = Calendar.getInstance();
					cEnd.setTime(end);
					/*
					 * while (cStart.before(cEnd)) { try { int dayOfWeek =
					 * cStart.get(Calendar.DAY_OF_WEEK); ResultSet stundenAmTag =
					 * StaticDatabase.db.getAllStundenplanStundenOnDay(idlehrer, dayOfWeek); while
					 * (stundenAmTag.next()) { String title = "TODO";
					 * 
					 * Time tStart = stundenAmTag.getTime("startzeit"); Time tEnd =
					 * stundenAmTag.getTime("endzeit");
					 * 
					 * Date startTime = HelpMethods.makeTime(cStart, tStart); Date endTime =
					 * HelpMethods.makeTime(cStart, tEnd);
					 * 
					 * Object concreteStundenplanStunde = new
					 * ConcreteStundenplanStunde(stundenAmTag.getInt("idstundenplaneinheit"),
					 * startTime); addEvent(new DefaultScheduleEvent(title, startTime, endTime,
					 * concreteStundenplanStunde)); }
					 * 
					 * } catch (SQLException e) { // TODO Auto-generated catch block
					 * e.printStackTrace(); }
					 * 
					 * cStart.add(Calendar.DATE, 1); }
					 */

				}
			};
		}

		return meinUnterrichtSchedule;
	}

	public void onEventSelect(SelectEvent selectEvent) {
		DefaultScheduleEvent event = (DefaultScheduleEvent) selectEvent.getObject();
		System.out.println(selectEvent.getObject());
		concreteStunde cs = (concreteStunde) event.getData();
		System.out.println(cs);
		try {
			Faces.redirect("unterrichtsstunde.xhtml?ue=" + cs.getUnterrichtseinheit().getIdunterrichtseinheit() + "&se="
					+ cs.getStundenplaneinheit().getIdstundenplaneinheit());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		 * ConcreteStundenplanStunde concreteStundenplanStunde =
		 * (ConcreteStundenplanStunde) event.getData(); String dateString = new
		 * SimpleDateFormat("yyyyMMddHHmmss").format(concreteStundenplanStunde.getDatum(
		 * )); String url ="klassenbucheintrag.xhtml?time=" + dateString + "&id=" +
		 * concreteStundenplanStunde.getIdStundenplanStunde() ;
		 * 
		 * try { FacesContext.getCurrentInstance().getExternalContext().redirect(url); }
		 * catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } FacesContext.getCurrentInstance().responseComplete();
		 */
	}

	public void setMeinUnterrichtSchedule(ScheduleModel meinUnterrichtSchedule) {
		this.meinUnterrichtSchedule = meinUnterrichtSchedule;

	}

	private ScheduleModel eventModel;

	private ScheduleModel lazyEventModel;

	private ScheduleEvent event = new DefaultScheduleEvent();

	/*
	 * @PostConstruct public void init() { eventModel = new DefaultScheduleModel();
	 * eventModel.addEvent(new DefaultScheduleEvent("Champions League Match",
	 * previousDay8Pm(), previousDay11Pm())); eventModel.addEvent(new
	 * DefaultScheduleEvent("Birthday Party", today1Pm(), today6Pm()));
	 * eventModel.addEvent(new DefaultScheduleEvent("Breakfast at Tiffanys",
	 * nextDay9Am(), nextDay11Am())); eventModel .addEvent(new
	 * DefaultScheduleEvent("Plant the new garden stuff", theDayAfter3Pm(),
	 * fourDaysLater3pm()));
	 * 
	 * lazyEventModel = new LazyScheduleModel() {
	 * 
	 * @Override public void loadEvents(Date start, Date end) { Date random =
	 * getRandomDate(start); addEvent(new DefaultScheduleEvent("Lazy Event 1",
	 * random, random));
	 * 
	 * random = getRandomDate(start); addEvent(new
	 * DefaultScheduleEvent("Lazy Event 2", random, random)); } }; }
	 */

	public Date getRandomDate(Date base) {
		Calendar date = Calendar.getInstance();
		date.setTime(base);
		date.add(Calendar.DATE, ((int) (Math.random() * 30)) + 1); // set random day of month

		return date.getTime();
	}

	public Date getInitialDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(calendar.get(Calendar.YEAR), Calendar.FEBRUARY, calendar.get(Calendar.DATE), 0, 0, 0);

		return calendar.getTime();
	}

	public ScheduleModel getEventModel() {
		return eventModel;
	}

	public ScheduleModel getLazyEventModel() {
		return lazyEventModel;
	}

	private Date previousDay8Pm() {
		Calendar t = (Calendar) today().clone();
		t.set(Calendar.AM_PM, Calendar.PM);
		t.set(Calendar.DATE, t.get(Calendar.DATE) - 1);
		t.set(Calendar.HOUR, 8);

		return t.getTime();
	}

	private Date previousDay11Pm() {
		Calendar t = (Calendar) today().clone();
		t.set(Calendar.AM_PM, Calendar.PM);
		t.set(Calendar.DATE, t.get(Calendar.DATE) - 1);
		t.set(Calendar.HOUR, 11);

		return t.getTime();
	}

	private Date theDayAfter3Pm() {
		Calendar t = (Calendar) today().clone();
		t.set(Calendar.DATE, t.get(Calendar.DATE) + 2);
		t.set(Calendar.AM_PM, Calendar.PM);
		t.set(Calendar.HOUR, 3);

		return t.getTime();
	}

	private Date nextDay9Am() {
		Calendar t = (Calendar) today().clone();
		t.set(Calendar.AM_PM, Calendar.AM);
		t.set(Calendar.DATE, t.get(Calendar.DATE) + 1);
		t.set(Calendar.HOUR, 9);

		return t.getTime();
	}

	private Date nextDay11Am() {
		Calendar t = (Calendar) today().clone();
		t.set(Calendar.AM_PM, Calendar.AM);
		t.set(Calendar.DATE, t.get(Calendar.DATE) + 1);
		t.set(Calendar.HOUR, 11);

		return t.getTime();
	}

	private Date fourDaysLater3pm() {
		Calendar t = (Calendar) today().clone();
		t.set(Calendar.AM_PM, Calendar.PM);
		t.set(Calendar.DATE, t.get(Calendar.DATE) + 4);
		t.set(Calendar.HOUR, 3);

		return t.getTime();
	}

	public ScheduleEvent getEvent() {
		return event;
	}

	public void setEvent(ScheduleEvent event) {
		this.event = event;
	}

	public void addEvent(ActionEvent actionEvent) {
		if (event.getId() == null)
			eventModel.addEvent(event);
		else
			eventModel.updateEvent(event);

		event = new DefaultScheduleEvent();
	}

	public void onDateSelect(SelectEvent selectEvent) {
		event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
	}

	private void addMessage(FacesMessage message) {
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
}
