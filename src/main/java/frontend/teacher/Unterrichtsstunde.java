package frontend.teacher;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import MySQL.StatDB;
import MySQL.model.Schueler;
import MySQL.model.Stundenplaneinheit;
import MySQL.model.Unterrichtseinheit;

@ManagedBean
@ViewScoped
public class Unterrichtsstunde implements Serializable {

	private List<Integer> lateList;
	private List<Integer> homeworkList;
	private List<Integer> missingList;
	String style="green";

	@PostConstruct
	public void init() {
		lateList = new ArrayList<>();
		homeworkList = new ArrayList<>();
		missingList = new ArrayList<>();
	}

	public List<Schueler> getSchuelerList(int id) {
		// return StatDB.dbi.get(Schueler.class, id);
		Schueler s1 = new Schueler(1, "Regine", "Leonhardt", null);
		Schueler s2 = new Schueler(2, "Walter", "Schüler", null);
		Schueler s3 = new Schueler(2, "Linus", "Geisler", null);
		Schueler s4 = new Schueler(2, "Kirsten", "Dahlen", null);
		Schueler s5 = new Schueler(2, "Yvonne", "Reiher", null);
		Schueler s6 = new Schueler(2, "Steven", "Osman", null);

		List<Schueler> rL = new ArrayList<Schueler>();
		rL.add(s1);rL.add(s2);rL.add(s3);rL.add(s4);rL.add(s5);rL.add(s6);
		return rL;
	}

	public void changeLate(int i) {
		change(lateList, i);
	}
	
	public void changeHomework(int i) {
		change(homeworkList, i);
	}
	
	public void changeMissing(int i) {
		change(missingList, i);
	}
	
	public String getStyleLate(int i) {
		return getStyle(lateList, i);
	}
	
	public String getStyleHomework(int i) {
		return getStyle(homeworkList, i);
	}
	
	public String getStyleMissing(int i) {
		return getStyle(missingList, i);
	}
	
	
	private void change(List<Integer> list, int i) {
		if(list.contains(i)) {
			list.remove(new Integer(i));
		} else {
			list.add(i);	
		}
	}
	
	private String getStyle(List<Integer> list, int i) {
		if(list.contains(i)) {
			System.out.println("r");
			return "red";
		} else {
			System.out.println("g");
			return "green";	
		}
	}


}
