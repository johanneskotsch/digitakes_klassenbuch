package frontend.teacher.model;

import MySQL.model.Stundenplaneinheit;
import MySQL.model.Unterrichtseinheit;

public class concreteStunde {

	private Unterrichtseinheit unterrichtseinheit;
	private Stundenplaneinheit stundenplaneinheit;

	public concreteStunde(Unterrichtseinheit unterrichtseinheit, Stundenplaneinheit stundenplaneinheit) {
		this.unterrichtseinheit = unterrichtseinheit;
		this.stundenplaneinheit = stundenplaneinheit;
	}

	public Unterrichtseinheit getUnterrichtseinheit() {
		return unterrichtseinheit;
	}

	public Stundenplaneinheit getStundenplaneinheit() {
		return stundenplaneinheit;
	}

	@Override
	public String toString() {
		return "concreteStunde [unterrichtseinheit=" + unterrichtseinheit + ", stundenplaneinheit=" + stundenplaneinheit
				+ "]";
	}
	
}
